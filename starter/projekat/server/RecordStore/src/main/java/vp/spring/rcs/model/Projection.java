package vp.spring.rcs.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Projection {


	@Id
	@GeneratedValue
	private Long id;
	
	private String date;
	private String time;
	
	private int numberOfFreeSeats;
	
	
	public int getNumberOfFreeSeats() {
		return numberOfFreeSeats;
	}
	public void setNumberOfFreeSeats(int numberOfFreeSeats) {
		this.numberOfFreeSeats = numberOfFreeSeats;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public Projection() {
		super();
	}
	
	
}

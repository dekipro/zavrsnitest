package vp.spring.rcs.data;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vp.spring.rcs.model.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long>{
	
	public Page<Movie> findAll(Pageable pageable);
	
	public List<Movie> findByTitleContainsAndGenreId(String title, Long genreId);


}

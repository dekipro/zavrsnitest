package vp.spring.rcs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vp.spring.rcs.data.ProjectionRepository;
import vp.spring.rcs.data.ReservationRepository;
import vp.spring.rcs.data.UserRepository;
import vp.spring.rcs.model.Projection;
import vp.spring.rcs.model.Reservation;
import vp.spring.rcs.model.user.SecurityUser;

@Service
public class ProjectionService {

	@Autowired
	ProjectionRepository repo;
	
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	ReservationRepository reservationRepository;
	
	@Autowired
	ProjectionRepository projectionRepository;
	
	public Projection findOne(Long id) {
		return repo.findOne(id);
	}
	
	public Projection save(Projection projection) {
		return repo.save(projection);
	}
	
	 public Reservation findByUserUsername(String username) {
		 Reservation r =  reservationRepository.findByUserUsername(username);
			if(r == null) {
				SecurityUser user = userRepo.findByUsername(username);
				if(user != null) {
					r = new Reservation();
					r.setUser(user);
					r = reservationRepository.save(r);
				}
			}
			return r;
		}

	
	public boolean addReservation(String username, int numberOfSeats,Long projectionId) {
		
		Reservation r = findByUserUsername(username);
        
		r.setProjection(projectionRepository.findOne(projectionId));
		
		r.getProjection().setNumberOfFreeSeats(r.getProjection().getNumberOfFreeSeats() - numberOfSeats);
		reservationRepository.save(r);
		return true;
	}

}

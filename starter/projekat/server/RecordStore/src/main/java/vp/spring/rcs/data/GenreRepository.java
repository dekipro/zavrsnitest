package vp.spring.rcs.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vp.spring.rcs.model.Genre;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Long>{

}

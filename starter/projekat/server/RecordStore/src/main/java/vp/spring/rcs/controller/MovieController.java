package vp.spring.rcs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.model.Movie;
import vp.spring.rcs.model.Projection;
import vp.spring.rcs.service.MovieService;
import vp.spring.rcs.service.ProjectionService;
import vp.spring.rcs.web.dto.CommonResponseDTO;

@RestController
@RequestMapping(value="/api/movies")
public class MovieController {

	@Autowired
	MovieService movieService;
	
	@Autowired
	ProjectionService projectionervice;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<Page<Movie>> findAll(Pageable pageable){
		Page<Movie> news = movieService.findAll(pageable);
		
		return new ResponseEntity<>(news, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<Movie> findOne(@PathVariable Long id){
		Movie movie = movieService.findOne(id);
		
		return new ResponseEntity<>(movie, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, params= {"title", "genreId"})
	public ResponseEntity<List<Movie>> findByTitleContainsAndCategoryId(@RequestParam String title, @RequestParam Long genreId){
		List<Movie> movies = movieService.findByTitleContainsAndGenreId(title, genreId);
		
		if(movies != null) {
			return new ResponseEntity<>(movies, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity<Movie> save(@RequestBody Movie movie){
		Movie m = movieService.save(movie);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	ResponseEntity<Movie> update(@PathVariable Long id, @RequestBody Movie movie){
		Movie m = movieService.findOne(id);
		m.setTitle(movie.getTitle());
		m.setSummary(movie.getSummary());
		m.setGenre(movie.getGenre());
		m = movieService.save(m);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}
	

	@RequestMapping(value = "{id}/add-projection", method = RequestMethod.PUT)
	public ResponseEntity<Movie> addComment(@PathVariable Long id,@RequestBody Projection projection) {
		Movie movie = movieService.findOne(id);
		Projection p = projectionervice.save(projection);
		movie.getProjections().add(p);
		return new ResponseEntity<>(movie, HttpStatus.CREATED);
	}


	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<CommonResponseDTO> delete(@PathVariable Long id){
		
		movieService.remove(id);
		
		return new ResponseEntity<>(new CommonResponseDTO("deleted"), HttpStatus.OK);
	}
}

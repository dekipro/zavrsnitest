package vp.spring.rcs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.model.Movie;
import vp.spring.rcs.model.Projection;
import vp.spring.rcs.service.ProjectionService;

@RestController
@RequestMapping()
public class ProjectionController {
	
	@Autowired
	ProjectionService projectionervice;

	@RequestMapping(value = "/api/projections/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Projection> addComment(@PathVariable Long id,@RequestParam int numberOfSeats) {
		Projection projection = projectionervice.findOne(id);
		projection.setNumberOfFreeSeats(projection.getNumberOfFreeSeats() - numberOfSeats);
		projection = projectionervice.save(projection);
		return new ResponseEntity<>(projection, HttpStatus.CREATED);
	}
}

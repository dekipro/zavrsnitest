package vp.spring.rcs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vp.spring.rcs.data.MovieRepository;
import vp.spring.rcs.model.Movie;

 
@Service
public class MovieService {

	@Autowired
	MovieRepository movieRepository;
	
	public Movie findOne(Long id) {
		return movieRepository.findOne(id);
	}
	
	public List<Movie> findAll(){
		return movieRepository.findAll();
	}
	
	public Page<Movie> findAll(Pageable page){
		return movieRepository.findAll(page);
	}
	
	public Movie save(Movie news) {
		return movieRepository.save(news);
	}
	
	public void remove(Long id) {
		movieRepository.delete(id);
	}
	
	public List<Movie> findByTitleContainsAndGenreId(String title, Long genreId) {
		return movieRepository.findByTitleContainsAndGenreId(title, genreId);
	}
}

package vp.spring.rcs.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vp.spring.rcs.model.Reservation;
import vp.spring.rcs.model.user.SecurityUser;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long>{
	  public Reservation findByUserUsername(String username);

}

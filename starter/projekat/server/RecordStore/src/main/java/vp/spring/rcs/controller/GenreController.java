package vp.spring.rcs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.model.Genre;
import vp.spring.rcs.service.GenreService;

@RestController
@RequestMapping(value="/api/genres")
public class GenreController {

	@Autowired
	GenreService genreService;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<Genre>> getAll(){
		List<Genre> genres = genreService.findAll();
		
		return new ResponseEntity<>(genres, HttpStatus.OK);
	}
}

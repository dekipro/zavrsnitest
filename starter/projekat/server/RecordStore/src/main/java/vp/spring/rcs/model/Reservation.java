package vp.spring.rcs.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import vp.spring.rcs.model.user.SecurityUser;

@Entity
public class Reservation {

	@Id
	@GeneratedValue
	private Long id;
	
	@OneToOne()
	private SecurityUser user;
	
	@ManyToOne(fetch=FetchType.EAGER)
	private Projection projection;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SecurityUser getUser() {
		return user;
	}

	public void setUser(SecurityUser user) {
		this.user = user;
	}

	public Projection getProjection() {
		return projection;
	}

	public void setProjection(Projection projection) {
		this.projection = projection;
	}

	public Reservation() {
		super();
	}

    

}

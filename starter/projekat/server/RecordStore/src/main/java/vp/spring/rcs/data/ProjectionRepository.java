package vp.spring.rcs.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vp.spring.rcs.model.Projection;

@Repository
public interface ProjectionRepository extends JpaRepository<Projection, Long>{

}

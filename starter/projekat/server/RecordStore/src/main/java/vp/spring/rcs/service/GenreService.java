package vp.spring.rcs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import vp.spring.rcs.data.GenreRepository;
import vp.spring.rcs.model.Genre;


@Service
public class GenreService {

	@Autowired
	GenreRepository repo;
	
	public List<Genre> findAll(){
		return repo.findAll();
	}
	
	
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {

  movie: any={
    title: '',
    summary: ''
  }

  numberOfSeats:number = 0;
  
  constructor(private route: ActivatedRoute, private http: HttpClient) {
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.http.get(`api/movies/${id}`).subscribe(response => {
        this.movie = response as any;
      });
    });
   }

  ngOnInit() {
  }

  loadData(){
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.http.get(`api/movies/${id}`).subscribe(response => {
        this.movie = response as any;
      });
    });
  }

 

  addReservation(p){
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const id = p.id;
    const params = new HttpParams()
      .set('numberOfSeats', id.toString())
      .set('projectionId', this.numberOfSeats.toString());
    
    this.http.post(`api/add-reservation`,{ params }, {headers}).subscribe((data: any) => {
      this.loadData();
    });
  }
}

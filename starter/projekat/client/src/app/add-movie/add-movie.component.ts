import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.css']
})
export class AddMovieComponent implements OnInit {

  @Output() movieAdded: EventEmitter<any> = new EventEmitter();  

   movie: any;
   genres: any[];

 
  constructor(private http: HttpClient, private router:Router) { 
    this.movie = {
      title:'',
      summary:''
     
    };
     
  }

  


  ngOnInit() {
    this.loadGenres();
  }

  loadGenres(){
    this.http.get('api/genres').subscribe( data => {
      this.genres = data as any[];
    });
  }

  addMovie(){
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.http.post('api/movies', JSON.stringify(this.movie), {headers}).subscribe((data: any) => {
      this.router.navigate(['/edit']);
    });
  }

  reset() {
    this.movie = {
      title: '',
      summary: ''
    };
  }


}

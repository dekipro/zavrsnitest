import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../security/authentication.service';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-edit-movie',
  templateUrl: './edit-movie.component.html',
  styleUrls: ['./edit-movie.component.css']
})
export class EditMovieComponent implements OnInit {

  movies: any[];

  movie: any={
    title: '',
    summary: ''
  }

  genres: any[];

  currentPage = 0;

  numberOfPages: number;

  constructor(private http: HttpClient, private authenticationService: AuthenticationService) {

   }


  ngOnInit() {
    this.loadData();
    this.loadGenres();
  }

  loadData() {
    const params = new HttpParams()
      .set('page', this.currentPage.toString())
      .set('size', '5');
    this.http.get('api/movies', { params }).subscribe( data => {
    // this.http.get(`api/computer-parts?page=${this.currentPage}&size=${5}`).subscribe( data => {
      this.movies = data['content'] as any[];
      this.numberOfPages = data['totalPages'];
      this.reset();
    });
  }

  loadGenres(){
    this.http.get('api/genres').subscribe( data => {
      this.genres = data as any[];
    });

  }

  hasRole(role: string): boolean {
    return this.authenticationService.hasRole(role);
  }

  filter(){
    const params = new HttpParams()
    .set('title', this.movie.title)
    .set('genreId', this.movie.genre.id.toString());
    this.http.get('api/movies', {params}).subscribe( data => {
      this.movies = data as any[];
    });
  }

  save() {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    if (this.movie.id === undefined) {
      this.http.post('api/movies', JSON.stringify(this.movie), {headers}).subscribe((data: any) => {
        this.loadData();
      });
    } else {
      this.http.put(`api/movies/${this.movie.id}`, JSON.stringify(this.movie), {headers}).subscribe((data: any) => {
        this.loadData();
      });
    }
  }

  delete(m: any) {
    this.http.delete(`api/movies/${m.id}`).subscribe(data =>{
      console.log(data);
      this.loadData();
    });
  }

  
  reset() {
    this.movie = {
      title: '',
      summary: ''
    };
  }

  byId(item1: any, item2: any) {
    if ( !item1 || !item2 ) {
      return false;
    }
    return item1.id === item2.id;
  }

  changePage(x: number) {
    if (this.currentPage + x >= 0 && this.currentPage + x < this.numberOfPages) {
      this.currentPage += x;
      this.loadData();
    }
  }

}

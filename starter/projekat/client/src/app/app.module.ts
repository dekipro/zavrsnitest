import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { MainComponentComponent } from './main-component/main-component.component';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationService } from './security/authentication.service';
import { CanActivateAuthGuard } from './security/can-activate-auth.guard';
import { JwtUtilsService } from './security/jwt-utils.service';
import { TokenInterceptorService } from './security/token-interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { MovieComponent } from './movie/movie.component';
import { EditMovieComponent } from './edit-movie/edit-movie.component';
import { AddMovieComponent } from './add-movie/add-movie.component';
import { UpdateComponent } from './update/update.component';
import { AddProjectionComponent } from './add-projection/add-projection.component';
import { CanActivateAdminService } from './security/can-activate-admin.service';
import { AddReservationComponent } from './add-reservation/add-reservation.component';

const routes: Routes = [
  {path: 'movie/:id', component: MovieComponent},
  {path: 'main', component: MainComponentComponent},
  {path: 'edit', component: EditMovieComponent, canActivate: [CanActivateAdminService]},
  {path: 'add', component: AddMovieComponent, canActivate: [CanActivateAdminService]},
  {path: 'update/:id', component: UpdateComponent, canActivate: [CanActivateAdminService]},
  {path: 'add-projection', component: AddProjectionComponent, canActivate: [CanActivateAdminService]},
  {path: 'login', component: LoginComponent},
  {path: '', redirectTo: 'main', pathMatch: 'full'},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    MainComponentComponent,
    LoginComponent,
    MovieComponent,
    EditMovieComponent,
    AddMovieComponent,
    UpdateComponent,
    AddProjectionComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(
      routes,
      { enableTracing: true }
    )
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
    AuthenticationService,
    CanActivateAuthGuard,
    JwtUtilsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

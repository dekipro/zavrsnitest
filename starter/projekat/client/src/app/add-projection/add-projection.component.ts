import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-add-projection',
  templateUrl: './add-projection.component.html',
  styleUrls: ['./add-projection.component.css']
})
export class AddProjectionComponent implements OnInit {

  movie:any;

  projection:any;


  constructor(private route: ActivatedRoute, private http: HttpClient,
    private router:Router) {
      this.route.params.subscribe(params => {
        const id = params['id'];
        this.http.get(`api/movies/${id}`).subscribe(response => {
          this.movie = response as any;
        });
      });

      this.projection = {
        date:'',
        time:''
       
      };
     }

  ngOnInit() {
  }

  addProjection(){
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.http.post(`${this.movie.id}/add-projection`, JSON.stringify(this.projection), {headers}).subscribe((data: any) => {
       this.reset();
    });
  }

  reset(){
    this.projection = {
      date:'',
      time:''
     
    };
  }
}

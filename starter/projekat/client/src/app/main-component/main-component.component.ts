import { Component, OnInit } from '@angular/core';
import { Response, RequestOptions,
         Headers } from '@angular/http';

import * as _ from 'lodash';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AuthenticationService } from 'app/security/authentication.service';

@Component({
  selector: 'app-main-component',
  templateUrl: './main-component.component.html',
  styleUrls: ['./main-component.component.css']
})
export class MainComponentComponent implements OnInit {

  movies: any[];

  movie: any={
    title: '',
    summary: ''
  }

  genres: any[];

  currentPage = 0;

  numberOfPages: number;

  ngOnInit(): void {
    this.loadData();
    this.loadGenres();
  }

  constructor(private http: HttpClient, private authenticationService: AuthenticationService) {
  }

  loadData() {
    const params = new HttpParams()
      .set('page', this.currentPage.toString())
      .set('size', '5');
    this.http.get('api/movies', { params }).subscribe( data => {
    // this.http.get(`api/computer-parts?page=${this.currentPage}&size=${5}`).subscribe( data => {
      this.movies = data['content'] as any[];
      this.numberOfPages = data['totalPages'];
      this.reset();
    });
  }

  loadGenres(){
    this.http.get('api/genres').subscribe( data => {
      this.genres = data as any[];
    });

  }

  hasRole(role: string): boolean {
    return this.authenticationService.hasRole(role);
  }

  filter(){
    const params = new HttpParams()
    .set('title', this.movie.title)
    .set('genreId', this.movie.genre.id.toString());
    this.http.get('api/movies', {params}).subscribe( data => {
      this.movies = data as any[];
    });
  }
  
  reset() {
    this.movie = {
      title: '',
      summary: ''
    };
  }

  changePage(x: number) {
    if (this.currentPage + x >= 0 && this.currentPage + x < this.numberOfPages) {
      this.currentPage += x;
      this.loadData();
    }
  }


}

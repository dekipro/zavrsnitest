import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  movie: any;


 genres:any[];

  constructor(private route: ActivatedRoute, private http: HttpClient,
     private router:Router) {
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.http.get(`api/movies/${id}`).subscribe(response => {
        this.movie = response as any;
      });
    });
   
   }

  ngOnInit() {
    this.loadGenres();
  }

  loadGenres(){
    this.http.get('api/genres').subscribe( data => {
      this.genres = data as any[];
    });
  }

  updateRecord() {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    
      this.http.put(`api/movies/${this.movie.id}`, JSON.stringify(this.movie), {headers}).subscribe((data: any) => {
        this.router.navigate(['/edit']);

      });
    }

    reset() {
      this.movie = {
        title: '',
        summary: ''
      };
    }
  }


